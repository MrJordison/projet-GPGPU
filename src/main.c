#include <stdio.h>
#include <math.h>
#include <string.h>

#include "include/exo1.h"
#include "include/exo2.h"
#include "include/exo3.h"
#include "include/exo4.h"
#include "include/exo5.h"
#include "include/exobonus.h"


/**
*   Il est possible d'appeler le main avec différents paramètres:
*   le premier, compris entre 1 et 6, permet de choisir quel exercice lancer (1 pour exo1,... et 6 pour exobonus.c)
*   le second permet de choisir le nombre 2^n de trapèzes à calculer (nombre de workitems), n étant le paramètre
*   le troisième est utilisé pour les kernels utilisant des workgroups, il définit ainsi le nombre de workgroups pour le test à lancer
*/
int main(int argc, char *argv[]){

	unsigned long time_cost;
	unsigned long timeMoy = 0;
	unsigned int nb = 50;
	
        //Vérification si l'utilisateur passe des paramètres au programme
        //pour choisir un exercice spécifique à lancer
        bool spec_execute = argc > 1 ;

	for (int i = 0; i < nb; i++)
	{ 
            if(spec_execute){

                //Choix exo1, sommes partielles d'éléments
                if(strcmp(argv[1],"1")==0){
                    printf("\rQuestion 1 (%d/%d) pour 2^%s workitems et %s workgroups",i,nb,argv[2],argv[3]);
                    exo1(pow(2,atoi(argv[2])),atoi(argv[3]),time_cost);
                }
                //Choix exo2, calcul séquentiel CPU de l'intégrale
                else if(strcmp(argv[1],"2")==0){
                    printf("\rQuestion 2 (%d/%d) pour 2^%s trapèzes",i,nb, argv[2]);
                    exo2(pow(2,atoi(argv[2])),0.f, 100.f,time_cost);
                }
                //Choix exo3, calcul des trapèzes avec kernel et somme séquentiel de l'intégrale sur CPU
                else if(strcmp(argv[1],"3")==0){
                    printf("\rQuestion 3 (%d/%d) pour 2^%s workitems",i,nb, argv[2]);
                    exo3(pow(2,atoi(argv[2])),0.f, 100.f,time_cost);
                }
                //Choix exo4, calcul des trapèzes et sommes partielles avec kernel, somme finale sur CPU
                else if(strcmp(argv[1],"4")==0){
                    printf("\rQuestion 4 (%d/%d) pour 2^%s workitems et %s workgroups",i,nb, argv[2], argv[3]);
                    exo4(pow(2,atoi(argv[2])),atoi(argv[3]),0.f, 100.f,time_cost);
                }
                //Choix exo5, calcul de l'ingralle uniquement avec kernel
                else if(strcmp(argv[1],"5")==0){
                    printf("\rQuestion 5 (%d/%d) pour 2^%s workitems et %s workgroups",i,nb, argv[2], argv[3]);
                    exo5(pow(2,atoi(argv[2])),atoi(argv[3]),0.f, 100.f,false,time_cost);
                }
                //Choix exobonus, sommes partielles d'éléments avec kernel utilisant les opérations atomiques
                else if(strcmp(argv[1],"6")==0){
                    printf("\rQuestion 6 (%d/%d) pour 2^%s workitems et %s workgroups",i,nb, argv[2], argv[3]);
                    exobonus(pow(2,atoi(argv[2])),atoi(argv[3]),time_cost);
                }
                else spec_execute = false ;
            }

            if(!spec_execute){
                //Choix par défaut pour lancer une fonction définie par l'utilisateur dans le code source, impose une recompilation à chaque changement
                printf("\rQuestion 5 (%d/%d) pour 2048 workitems et 16 workgroups",i,nb);
                exo5(2048, 16, 0.f, 100.f, false, time_cost);	
            }
            //ajout du temps d'exécution pour ensuite calculer la moyenne
            timeMoy += time_cost;

            //affichage du temps pour le test courant
            printf(" : %lu ns",time_cost);
	}
	
	timeMoy /= nb;

	printf("\nTemps moyen d'exécution sur %d passes : %lu ns\n", nb ,timeMoy);
	
}
