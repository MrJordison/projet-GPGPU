#ifndef EXO5_H
#define EXO5_H

#include <stdio.h>
#include <stdlib.h>

/**
*   Question 5
*   Implémentation du kernel de calcul d'intégrale avec sommes partielles et la somme finale(linéaire et parallèle). Le pas h sera calculé en en fonction de l'intervalle et du nombre de trapèzes passés en paramètres .
*   @global_size le nombre de trapèzes calculés
*   @workgroup_size le nombre de workgroups utilisés
*   @begin la valeur de début de l'intervalle
*   @end la valeur de fin de l'intervalle
*   @type_sum permet de choisir si la somme finale sera faite de façon linéaire par un seul workitem ou de façon parallèle (avec workitem par workgroup). Si @type_sum est égal à true, la somme finale sera parallèle, sinon elle sera linéaire .
*   @time_cost une variable de temps passée par référence, on sera stocké le temps d'exécution du programme (temps d'exécution des sommes partielles et de la somme finale)
*   @return la valeur de l'intégrale
*/
float exo5(int global_size, int workgroup_size, float begin, float end, bool type_sum, unsigned long & time_cost);

#endif
