#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>

//Variable permettant d'activer ou non un affichage supplémentaire dans les différents exos pour débuggage
#define DEBUG_MODE 0

/**
*   Fonction permettant d'importer un kernel stocké dans un fichier séparé
*
*   @filename le nom du fichier à importer
*   @return le contenu du kernel dans un tableau de caractères
*
*/
char* import_kernel(char* filename);

#endif
