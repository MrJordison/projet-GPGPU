#ifndef EXO4_H
#define EXO4_H

#include <stdio.h>
#include <stdlib.h>

/**
*   Question 4
*   Implémentation du kernel de calcul d'intégrale dont les sommes partielles faites via les workgroups. La somme finale est exécutée par le CPU. Le pas h sera calculé en en fonction de l'intervalle et du nombre de trapèzes passés en paramètres .
*   @global_size le nombre de trapèzes calculés
*   @workgroup_size le nombre de workgroups utilisés
*   @begin la valeur de début de l'intervalle
*   @end la valeur de fin de l'intervalle
*   @time_cost une variable de temps passée par référence, on sera stocké le temps d'exécution du programme (temps d'exécution des sommes partielles et de la somme finale)
*   @return la valeur de l'intégrale
*/
float exo4(int global_size, int workgroup_size, float begin, float end, unsigned long & time_cost);

#endif
