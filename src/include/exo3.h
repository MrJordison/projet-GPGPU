#ifndef EXO3_H
#define EXO3_H

#include <stdio.h>
#include <stdlib.h>

/**
*   Question 3
*   Implémentation du kernel de calcul d'intégrale simple, calculant simplement les aires des trapèzes avant de les retourner au CPU qui fera la somme finale. Le pas h sera calculé en en fonction de l'intervalle et du nombre de trapèzes passés en paramètres .
*   @global_size le nombre de trapèzes calculés
*   @begin la valeur de début de l'intervalle
*   @end la valeur de fin de l'intervalle
*   @time_cost une variable de temps passée par référence, on sera stocké le temps d'exécution du programme (temps d'exécution des sommes partielles et de la somme finale)
@   @return la valeur de l'intégrale
*/
float exo3(int global_size, float begin, float end, unsigned long & time_cost);

#endif
