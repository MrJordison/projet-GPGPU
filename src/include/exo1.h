#ifndef EXO1_H
#define EXO1_H

#include <stdio.h>
#include <stdlib.h>

/**
*   Question 1
*   Implémentation du kernel de réduction vu en cours. Effectue @workgroup_size sommes partielles sur un tableau de @global_size valeurs .
*   @global_size le nombre de workitems
*   @workgroup_size le nombre de workgroups
*   @time_cost une variable de temps passée par référence, on sera stocké le temps d'exécution du programme (temps d'exécution des sommes partielles et de la somme finale)
*   @return la valeur de l'intégrale
*/
float exo1(int global_size, int workgroup_size, unsigned long & time_cost);

#endif
