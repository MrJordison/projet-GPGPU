#ifndef USUAL_FCOH
#define USUAL_FCOH

#include <stdlib.h>
#include <stdio.h>

/**
*   Fonction usuelle utilisée pour le calcul d'intégrale. Elle est appelée pour générer l'échantillonage
*   de valeurs sur un intervalle donné.
*   @x la valeur en float dont souhaite calculer f(x)
*   @return la valeur f(x)
*/
float usual_function(float x);

#endif
