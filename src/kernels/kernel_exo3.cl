//Kernel répondant à la question 3, calculant simplement
// l'aire pour chaque trapèze par un workitem et le stocke
// dans le tableau d'output

__kernel void simple_integrale_kernel(
	__global float* output, __global float* input, float h){
	
	unsigned int globalId = get_global_id(0) ;

	output[globalId] =  h * ((input[globalId] + input[globalId+1]) / 2);
}
