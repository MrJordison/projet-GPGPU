#include "include/utils.h"

char* import_kernel(char* filename){

    FILE* programHandle;
    size_t programSize;
    char *programBuffer;

    //récupération de la taille du kernel
    programHandle = fopen(filename, "r");

    //test si le fichier a bien été trouvé et chargé
    //Si ce n'est pas le cas, un message est affiché et le programme se termine .
    if(programHandle == NULL){
        printf("Erreur, le kernel à charger est introuvable.\n");
        exit(1) ;
    }

    fseek(programHandle, 0, SEEK_END);
    programSize = ftell(programHandle);
    rewind(programHandle);

    //stockage du kernel dans un tableau de char*
    programBuffer = (char*) malloc(programSize + 1);
    fread(programBuffer, sizeof(char), programSize, programHandle);
    programBuffer[programSize] = '\0';
    programSize += 1 ;
    fclose(programHandle);

    //retourne le tableau de caractères rempli
    return programBuffer ;
}
