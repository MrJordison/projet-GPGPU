#include "include/exobonus.h"

#include <CL/cl.h>
#include <math.h>
#include <time.h>

#include "include/utils.h"

float exobonus(int global_size, int workgroup_size, unsigned long & time_cost) {

    //instanciation des différents paramètres et valeurs utilisés par le kernel
    int array_input[global_size] ;
    int array_output[workgroup_size] ; 
    int reduce_elem_nb = (int) (global_size / workgroup_size) ; 

    //initialisation du tableau de valeurs d'entrée
    for(int i = 0 ; i < global_size ; ++i)
        array_input[i] = i + 1 ;

    //initialisation du tableau de valeurs de sortie
    for(int i = 0 ; i < workgroup_size ; ++i)
        array_output[i] = 0 ;
                    
    //import du kernel dans un tableau de caractères depuis un fichier externe
    char* programSource = import_kernel("kernels/kernel_bonus.cl");
                    
    // Use this to check the output of each API call
    cl_int status; 

////////////////////////// STEP 1 //////////////////////////////////////////

    cl_uint numPlatforms = 0;
    cl_platform_id *platforms = NULL;

    // Use clGetPlatformIDs() to retrieve the number of
    // platforms
    status = clGetPlatformIDs(0, NULL, 
                                        &numPlatforms);
    // Allocate enough space for each platform
    platforms = (cl_platform_id*)malloc(numPlatforms * sizeof(cl_platform_id));

    // Fill in platforms with clGetPlatformIDs()
    status = clGetPlatformIDs(numPlatforms, platforms, NULL);

////////////////////////// STEP 2 //////////////////////////////////////////

    cl_uint numDevices = 0;
    cl_device_id *devices = NULL;

    // Use clGetDeviceIDs() to retrieve the number of 
    // devices present
    status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, 0, NULL, &numDevices);

    // Allocate enough space for each device
    devices = (cl_device_id*)malloc(numDevices *
                                                 sizeof(cl_device_id));
    // Fill in devices with clGetDeviceIDs()
    status = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_ALL, numDevices, devices, NULL);

////////////////////////// STEP 3 //////////////////////////////////////////

    cl_context context = NULL;

    // Create a context using clCreateContext() and 
    // associate it with the devices
    context = clCreateContext(NULL, numDevices,devices, NULL, NULL, &status);

////////////////////////// STEP 4 //////////////////////////////////////////

    cl_command_queue cmdQueue;

    // Create a command queue using
    // clCreateCommandQueue(), and associate it with
    // the device you want to  execute on
    cmdQueue = clCreateCommandQueue(context, devices[0], CL_QUEUE_PROFILING_ENABLE, &status);

////////////////////////// STEP 5 //////////////////////////////////////////

    cl_mem bufferA;  // Input array on the device
    cl_mem bufferC;  // Output array on the device

    // Use clCreateBuffer() to create buffer objects 
    // that will contain the data from the host arrays
    bufferC = clCreateBuffer(context, CL_MEM_WRITE_ONLY, workgroup_size*sizeof(int), NULL, &status);
    bufferA = clCreateBuffer(context, CL_MEM_READ_ONLY, global_size*sizeof(int), NULL, &status);

////////////////////////// STEP 6 //////////////////////////////////////////

    // Use clEnqueueWriteBuffer() to write input array A
    // to the device buffer bufferA
    status = clEnqueueWriteBuffer(cmdQueue, bufferA, CL_FALSE, 0, global_size*sizeof(int), array_input, 0, NULL, NULL);

////////////////////////// STEP 7 //////////////////////////////////////////

    // Create a program using
    // clCreateProgramWithSource()
    cl_program program = clCreateProgramWithSource(context, 1, (const char **) &programSource, NULL, &status);

    // Build (compile) the program for the devices with
    // clBuildProgram()
    status = clBuildProgram(program, numDevices, devices, NULL, NULL, NULL);

    //Si problème à la compilation ou exe du kernel, renvoie d'un code erreur et affichage du log d'OpenCL
    if(status != CL_SUCCESS){

        size_t len;
        clGetProgramBuildInfo(program,devices[0],CL_PROGRAM_BUILD_LOG,0,NULL,&len);

        char* buffer = (char*)malloc(len);
        clGetProgramBuildInfo(program,devices[0], CL_PROGRAM_BUILD_LOG,len,buffer,NULL);
        printf("%s\n",buffer);

        //sortie du programme
        exit(1);
    }

////////////////////////// STEP 8 //////////////////////////////////////////

    cl_kernel kernel = NULL;

    // Use clCreateKernel() to create a kernel
    kernel = clCreateKernel(program, "reduction_kernel", &status);

////////////////////////// STEP 9 //////////////////////////////////////////

    // Associate the input and output buffers with the 
    // kernel using clSetKernelArg()
    status  = clSetKernelArg(kernel, 0, sizeof(int), &global_size);
    status |= clSetKernelArg(kernel, 1, sizeof(cl_mem), &bufferA);
    status |= clSetKernelArg(kernel, 2, sizeof(cl_mem), &bufferC);
    status |= clSetKernelArg(kernel, 3, global_size*sizeof(int), NULL);

////////////////////////// STEP 10 //////////////////////////////////////////

    // Define an index space (global work size) of work
    // items for execution. A workgroup size (local work
    // size) is not required, but can be used.
    size_t localWorkSize[1], globalWorkSize[1];

    // There are 'elements' work-items
    localWorkSize[0] = reduce_elem_nb ;
    globalWorkSize[0] = global_size ;

////////////////////////// STEP 11 //////////////////////////////////////////

    cl_event timing_event ;
    cl_int err_code ;

    // Execute the kernel by using
    // clEnqueueNDRangeKernel().
    // 'globalWorkSize' is the 1D dimension of the
    // work-items
    status = clEnqueueNDRangeKernel(cmdQueue, kernel, 1, NULL, globalWorkSize, localWorkSize, 0, NULL, &timing_event) ;

    //Calcul du temps d'exécution des kernels
    clFinish(cmdQueue);
    cl_ulong startTime, endTime ;
    err_code = clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_START,sizeof(cl_ulong),&startTime, NULL);
    err_code = clGetEventProfilingInfo(timing_event, CL_PROFILING_COMMAND_END,sizeof(cl_ulong),&endTime, NULL);

    time_cost = (unsigned long)(endTime - startTime);
	
////////////////////////// STEP 12 //////////////////////////////////////////

    // Use clEnqueueReadBuffer() to read the OpenCL
    // output buffer (bufferC) to the host output array (C)
    clEnqueueReadBuffer(cmdQueue, bufferC, CL_TRUE, 0, workgroup_size*sizeof(int), array_output, 0, NULL, NULL);

    //Calcul de la somme sur le tableau de sortie pour obtenir la valeur finale
    int sum_value = 0 ;

    clock_t timeBegin, timeEnd;
    timeBegin = clock();

    for(int i = 0; i < workgroup_size ; ++i){
        sum_value += array_output[i] ;
    }

    //Ajout du temps CPU au temps kernel pour avoir le temps d'exécution total 
    timeEnd = clock();
    float timeCPU = ((float)(timeEnd - timeBegin) / CLOCKS_PER_SEC);
    if(DEBUG_MODE) printf("\ntemps avant + cpu : %lu\n",time_cost);
    time_cost += (unsigned long)(timeCPU * pow(10,9));
    if(DEBUG_MODE) printf("temps apres + cpu : %lu\n",time_cost);

    if(DEBUG_MODE){
        //Affichage du nombre de workitems géré par chaque workgroup
        printf("Nombre de workitems par workgroup : %d\n",reduce_elem_nb);
        //affichage des valeurs retournées par le kernel pour chaque workgroup
        for(int i  = 0 ; i < workgroup_size ; ++i)
            printf("Valeur retournée par workgroup n°%d : %d\n",i,array_output[i]);

        // Calcul de la somme théorique avec la formule de somme des n premiers entiers
        int seq_sum_value = ((global_size * (global_size + 1) )/ 2) ;
        printf("Valeur kernel : %d\t Valeur attendue : %d\n",sum_value,seq_sum_value);

        //Vérification de la valeur calculée par kernel
        if(sum_value == seq_sum_value)
            printf("Valeur obtenue correcte\n") ;
        else
            printf("Valeur obtenue incorrecte\n") ;
    }

////////////////////////// STEP 13 //////////////////////////////////////////

    // Free OpenCL ressources
    clReleaseKernel(kernel);
    clReleaseProgram(program);
    clReleaseCommandQueue(cmdQueue);
    clReleaseMemObject(bufferA);
    clReleaseMemObject(bufferC);
    clReleaseContext(context);

    // Free host ressources
    free(platforms);
    free(devices);
    free(programSource);

    return sum_value;
}

