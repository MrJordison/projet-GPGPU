#include "include/exo2.h"

#include <time.h>
#include <math.h>

#include "include/utils.h"
#include "include/usual_function.h"

float exo2(int global_size, float begin, float end, unsigned long & time_cost){
	
    //instanciation des différents paramètres et valeurs utilisés
    float h = (end - begin) / global_size ;
    
    //génération des valeurs d'échantillonage générée par la fonction usuelle dont l'intégralesera calculée.
    float echantillonage[global_size + 1] ;

    for(int i = 0 ; i <= global_size ; ++i)
        echantillonage[i] = usual_function((i * h) + begin) ;

    //initialisation du tableau de résultats à retourner
    float res = 0.f ;

    //Initialisation des variables pour la mesure du temps CPU
    clock_t timeBegin, timeEnd ; 
    timeBegin = clock();

    //ajout de l'aire de chaque trapèze au résultat final
    for(int i = 0 ; i < global_size ; ++i)
        res += h * ((echantillonage[i] + echantillonage[i + 1]) / 2) ;
    
    //Résolution et affichage du temps CPU
    timeEnd = clock() ;
    float time = ((float)(timeEnd - timeBegin) / CLOCKS_PER_SEC);
    time_cost = (unsigned long)(time * pow(10,9));

    if(DEBUG_MODE)
        printf("\nValeur séquentielle CPU calculée : %f\n",res);
    
    return res ;
}


